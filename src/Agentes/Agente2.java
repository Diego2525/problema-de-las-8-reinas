package Agentes;

import Arbol.arbol;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Agente2 extends Agent implements Serializable {

    protected static ArrayList mensajes = new ArrayList<ACLMessage>();

    @Override
    protected void setup() {
        // TODO Auto-generated method stub
        addBehaviour(new Comportamiento());
        super.setup();
    }

    @Override
    protected void takeDown() {
        // TODO Auto-generated method stub
        System.out.println(getName() + " murio!!!");
        super.takeDown();
    }

    class Comportamiento extends Behaviour {

        boolean in = true;
        int val = 0;
        boolean bandera = false;

        @Override
        public void action() {

            ACLMessage mensaje = blockingReceive();
            LinkedList<int []> ancho=new LinkedList<>();
            Arbol.arbol ar = new arbol();
            ar.generarHijos(ar.getRoot(),0);
            try {
                ancho=ar.buscarEnAnchura((int[]) mensaje.getContentObject());
            } catch (UnreadableException ex) {
                Logger.getLogger(Agente2.class.getName()).log(Level.SEVERE, null, ex);
            }
         
            enviarMensaje(ancho, mensaje.getSender().getLocalName(), mensaje.getConversationId());
            bandera=true;

        }

        private void enviarMensaje(Object contenido, String receptor, String idConversacion) {
            AID id = new AID();
            id.setLocalName(receptor);
            ACLMessage acl = new ACLMessage(ACLMessage.REQUEST);
            acl.addReceiver(id);
            acl.setSender(getAID());
            try {
                acl.setContentObject((Serializable) contenido);
            } catch (IOException ex) {
                Logger.getLogger(Agente2.class.getName()).log(Level.SEVERE, null, ex);
            }
            acl.setLanguage("lol");
            acl.setConversationId(idConversacion);
            send(acl);
        }

        @Override
        public boolean done() {

            return bandera;
        }

    }

}
