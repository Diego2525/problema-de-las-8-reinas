package Agentes;

import Vista.Ani;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Vista.MejorCamino;
import Vista.QuicksortG;
import Vista.Work;
import javax.swing.JFrame;

public class Agente1 extends Agent implements Serializable {

    protected static ArrayList mensajes = new ArrayList<ACLMessage>();

    @Override
    protected void setup() {
        // TODO Auto-generated method stub
        addBehaviour(new Comportamiento());
        super.setup();
    }

    @Override
    protected void takeDown() {
        // TODO Auto-generated method stub
        System.out.println(getName() + " murio!!!");
        super.takeDown();
    }

    class Comportamiento extends Behaviour {

        boolean in = true;
        int estadoInicial[] = {1, 3, 6, 0, 0, 0, 0, 0};
        boolean bandera = false;
        @Override
        public void action() {

            

            if (in) {
                enviarMensaje(estadoInicial, "Agente2", "id12");
                enviarMensaje(estadoInicial, "Agente3", "id13");
                enviarMensaje(estadoInicial, "Agente4", "id14");
                in = false;
            }

            ACLMessage mensaje = blockingReceive();
            if (mensajes.size() < 4) {
                mensajes.add(mensaje);
            }

            if (mensajes.size() > 2) {
                ACLMessage menjs[] = new ACLMessage[3];
                for (int i = 0; i < mensajes.size(); i++) {
                    menjs[i] = (ACLMessage) mensajes.get(i);
                }
                LinkedList<int[]> op1 = null;
                LinkedList<int[]> op2 = null;
                LinkedList<int[]> op3 = null;
                try {
                    op1 = (LinkedList<int[]>) menjs[0].getContentObject();
                    op2 = (LinkedList<int[]>) menjs[1].getContentObject();
                    op3 = (LinkedList<int[]>) menjs[2].getContentObject();
                } catch (UnreadableException ex) {
                    Logger.getLogger(Agente1.class.getName()).log(Level.SEVERE, null, ex);
                }
                ArrayList<int[]> MCamino = new ArrayList<>();
                for (int i = 0; i < op3.size(); i++) {
                    MCamino.add(op3.get(i));
                }

                MejorCamino mc = new MejorCamino(op3);
                mc.setVisible(true);
//                System.out.println("Cantidad d "+ MCamino.size());
                //  mc.armarCamino(MCamino);
//                JFrame frame = new JFrame("8 Reinas");
//                frame.setSize(620, 620);
//                frame.getContentPane().add(new Work(op3.get(0)));
//                frame.setLocationRelativeTo(null);
//                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//                frame.setVisible(true);
////                for (int i = 0; i < op3.size(); i++) {
//                    new Ani(frame).actualizar(op3.get(2));
////                }
//
//                bandera = true;
//                System.out.println("hola");
////                
            }
        }

        private void enviarMensaje(Object contenido, String receptor, String idConversacion) {
            AID id = new AID();
            id.setLocalName(receptor);
            ACLMessage acl = new ACLMessage(ACLMessage.REQUEST);
            acl.addReceiver(id);
            acl.setSender(getAID());
            try {
                acl.setContentObject((Serializable) contenido);
            } catch (IOException ex) {
                Logger.getLogger(Agente1.class.getName()).log(Level.SEVERE, null, ex);
            }
//			acl.setContent(contenido);
            acl.setLanguage("lol");
            acl.setConversationId(idConversacion);
            send(acl);
        }

        @Override
        public boolean done() {

            return bandera;
        }

    }

}
