package Contenedor;

import Agentes.*;

import jade.wrapper.AgentContainer;
import jade.wrapper.StaleProxyException;
import jade.core.Profile;
import jade.core.ProfileImpl;

public class Contenedor {

    public static void main(String[] args) {
        new Contenedor().contenedor();
    }

    AgentContainer mainContainer;

    public void contenedor() {
        jade.core.Runtime runtime = jade.core.Runtime.instance();
        runtime.setCloseVM(true);
        Profile profile = new ProfileImpl(null, 1099, null);
        mainContainer = runtime.createMainContainer(profile);
        iniciarAgentes();
    }

    private void iniciarAgentes() {

        try {
            mainContainer.createNewAgent("Agente2", Agente2.class.getName(), null).start();
            mainContainer.createNewAgent("Agente3", Agente3.class.getName(), null).start();
            mainContainer.createNewAgent("Agente4", Agente4.class.getName(), null).start();
            mainContainer.createNewAgent("Agente1", Agente1.class.getName(), null).start();
        } catch (StaleProxyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
