package Vista;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class QuicksortG {

    JFrame frame;

    public QuicksortG(JFrame frame) {
        this.frame = frame;
    }

    public void swap(int a, int b, int arr[]) {
        if(a==b)return;
        int aux = arr[a];
        arr[a] = arr[b];
        arr[b] = aux;
        frame.getContentPane().add(new Work(arr));
        try {
            Thread.sleep(250);
        } catch (InterruptedException ex) {
            //Logger.getLogger(HeapSort.Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pivot(int in, int fi, int arr[]) {
        int piv = in, l = in, r = fi;
        while (r >= l) {
            while (r >= l) {
                if (arr[piv] <= arr[l]) {
                    swap(piv, l, arr);
                    piv = l;
                    l++;
                    break;
                } else {
                    l++;
    
                }
            }

            while (r >= l) {
                if (arr[piv] >= arr[r]) {
                    swap(piv, r, arr);
                    piv = r;
                    r--;
                    break;
                } else {
                    r--;
                }
            }
        }
        if ((fi - in) > 0) {
            if (piv > in) {
                pivot(in, piv - 1, arr);
            }
            if (piv < fi) {
                pivot(piv + 1, fi, arr);
            }
        }
    }

    public void quicksort(int arr[]) {
        pivot(0, arr.length - 1, arr);
    }
}
