package Vista;

import Agentes.Agente1;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.LinkedList;

public class Work extends JPanel {

    int[] pos;

    public Work() {
    }

    public Work(int[] pos) {
        this.pos = pos;
    }

    public void dama(Graphics g, int x, int y) {
        g.setColor(Color.BLUE);
        int ancho = 49;
        int aux = 0;
        while (ancho >= 1) {
            aux = (50 - ancho) / 2;
            g.drawOval(x + aux, y + aux, ancho, ancho);
            ancho = ancho - 2;
        }
    }

    public void todas(Graphics g, int[] pos) {
        for (int i = 0; i < pos.length; i++) {
            if (pos[i] == 0) {
                continue;
            }
            dama(g, 50 + 50 * (i + 1), 50 + 50 * (int) pos[i]);
        }
    }

    public void paint(Graphics g) {

        g.fillRect(100, 100, 400, 400);
        for (int i = 100; i <= 400; i += 100) {
            for (int j = 100; j <= 400; j += 100) {
                g.clearRect(i, j, 50, 50);
            }
        }

        for (int i = 150; i <= 450; i += 100) {
            for (int j = 150; j <= 450; j += 100) {
                g.clearRect(i, j, 50, 50);
            }
        }
        todas(g, pos);
        repaint();
    }

//    private void formMouseClicked(java.awt.event.MouseEvent evt) {
//        System.out.println("Hola");        // TODO add your handling code here:
//    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("8 Reinas");
        frame.setSize(620, 620);
        LinkedList v = new LinkedList();
        frame.getContentPane().add(new Work());
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
