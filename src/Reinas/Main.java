
package Reinas;

import static Reinas.ArbolR.n;

public class Main {
    int estado[]=new int[n];
    ArbolR arbol = new ArbolR(estado);

    public Main() {
//        ArbolR arbol = new ArbolR(estado);
        arbol.resolver();
    }
    
    public static void main(String[] args) {
        Main n = new Main();
//        n.arbol.imprimirS();
//        System.out.println("Recorrido por profundidad");
        n.arbol.imprimirProfundidad(n.arbol.root,""+0);
//        System.out.println("Recorrido por anchura");
//        n.arbol.anchura(n.arbol.root);
        System.out.println("Solucion\n"+"--------");
        n.arbol.imprimirS();
        n.arbol.tamS();
    }
}
