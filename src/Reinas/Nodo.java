/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reinas;

import static Reinas.ArbolR.n;
import java.util.LinkedList;

/**
 *
 * @author Cristian
 */
public class Nodo {

    int[] estado = new int[n];
    LinkedList<Nodo> hijos;
    Nodo padre;
    boolean recorrido;
    String nombre;

    public Nodo(int estado[], Nodo padre) {
        this.estado = estado;
        hijos = new LinkedList<>();
        this.padre = padre;
    }

    public int[] getEstado() {
        return estado;
    }

    public void setEstado(int[] estado) {
        this.estado = estado;
    }

    public LinkedList<Nodo> getHijos() {
        return hijos;
    }
    
    public int[] getReinas() {
        return estado;
    }

    public void setHijos(LinkedList<Nodo> hijos) {
        this.hijos = hijos;
    }

 

}
