package Reinas;

//import java.util.LinkedList;
import java.util.LinkedList;
import java.io.Serializable;
import java.util.Queue;

public class ArbolR implements Serializable {

    static int n = 4;

    public static LinkedList<int[]> getSoluciones() {
        return soluciones;
    }

    public void tamS() {
        System.out.println(soluciones.size());
    }

    Nodo root = null;
    public static LinkedList<int[]> soluciones;

    public ArbolR(int[] estado) {
        root = new Nodo(estado, (Nodo) null);
        soluciones = new LinkedList<>();
    }

    public boolean validar(Nodo tab, int pos) {
        int tablero[] = tab.estado;
        for (int i = 0; i < pos; i++) {
            for (int j = i + 1; j < pos; j++) {
                if ((tablero[i]) == (tablero[j]) || Math.abs(i - j) == Math.abs(tablero[i] - tablero[j])) {
                    return false;
                }
            }
        }
        return true;
    }
    
      public boolean validar(int []tablero,int pos) {
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if ((tablero[i]) == (tablero[j]) || Math.abs(i - j) == Math.abs(tablero[i] - tablero[j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public void imprimirMatriz(int[] est) {
        int[][] matriz = new int[n][n];
        for (int i = 0; i < n; i++) {
            if (est[i] != 0) {
                matriz[est[i] - 1][i] = 1;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    public void imprimirS() {
        for (int i = 0; i < soluciones.size(); i++) {
            imprimirMatriz(soluciones.get(i));
        }
    }

    public void imprimirProfundidad(Nodo tab, String n) {
        if (tab == null) {
            return;
        }
        for (int i = 0; i < tab.hijos.size(); i++) {
            System.out.println(n);
            imprimirProfundidad(tab.getHijos().get(i),n+i);
            imprimirMatriz(tab.hijos.get(i).getEstado());
           
        }
    }

    public void algNodos(Nodo tab, int pos) {
        if (validar(tab, pos)) {
            if (pos < n) {
                for (int i = 0; i < n; i++) {
                    int[] est = new int[n];
                    for (int j = 0; j < n; j++) {
                        est[j] = tab.estado[j];
                    }
                    est[pos] = i + 1;
                    Nodo nhijo = new Nodo(est, tab);
                    tab.hijos.addLast(nhijo);
                    algNodos(nhijo, pos + 1);
                }
            } else {
                soluciones.addLast(tab.estado);
            }
        }
    }

    public void resolver() {
        algNodos(root, 0);
    }
    
    public boolean numeroReinas(int[] matriz) {
        int aux = 0;
        for (int i = 0; i < matriz.length; i++) {
                if (matriz[i] != 0) {
                    aux++;
                }
        }
        if (aux == 8) {
            return true;

        } else {
            return false;

        }
    }

    public int anchura(Nodo raiz) {
        Queue<Nodo> cola = new LinkedList();
        cola.add(raiz);
        int conta = 0;
        while (!cola.isEmpty()) {
            raiz = cola.element();
            conta++;
            imprimirMatriz(raiz.estado);
            cola.remove();
            for (int i = 0; i < raiz.hijos.size(); i++) {
                if (!raiz.hijos.get(i).recorrido) {
                    cola.add(raiz.hijos.get(i));
                    raiz.hijos.get(i).recorrido = true;
                }
            }

        }
        return conta;
    }
}
